! ---------------------------------------------
! Zelo*s Whitelist
! ---------------------------------------------
! GitHub: https://github.com/Zelo72
! Mail:   zelo72@dismail.de
! ---------------------------------------------

! AdBlock-Detection (Bild/Sport1/...)
! *.de.ioam.de
! de.ioam.de
@@||script.ioam.de^
@@||tagger.opecloud.com^
@@||tags.tiqcdn.com^
@@||www.asadcdn.com^

! Amazon
@@/*.media-amazon.com/
@@||aan.amazon.com^
@@||aax-eu-retail-direct.amazon-adsystem.com^
@@||aax-eu.amazon-adsystem.com^
@@||aax.amazon-adsystem.amazon.com^
@@||aax.amazon-adsystem.com^
@@||aviary.amazon.de^
@@||c.amazon-adsystem.com^
@@||fls-eu.amazon-adsystem.com^
@@||fls-eu.amazon.ae^
@@||fls-eu.amazon.co.uk^
@@||fls-eu.amazon.com^
@@||fls-eu.amazon.de^
@@||fls-eu.amazon.es^
@@||fls-eu.amazon.fr^
@@||fls-eu.amazon.in^
@@||fls-eu.amazon.it^
@@||fls-eu.amazon.nl^
@@||fls-eu.amazon.sa^
@@||fls-eu.amazon.se^
@@||mads-eu.amazon.com^
@@||mads.amazon-adsystem.com^
@@||global.safebrowsing.service.amazonsilk.com^
@@||configuration-vending.service.amazonsilk.com^

! Amazon Devices (Alexa, Kindle, ...)
@@||d3p8zr0ffa9t17.cloudfront.net^
@@||data.alexa.com^
@@||device-metrics-us-2.amazon.com^
@@||device-metrics-us.amazon.com^
@@||dxz5jxhrrzigf.cloudfront.net^
@@||msh.amazon.co.uk^
@@||msh.amazon.com^
@@||msh.amazon.de^
@@||prod.amcs-tachyon.com^

! Amazon Prime Video
@@/*.aiv-cdn.net/
@@/*.aiv-cdn.net.c.footprint.net/
@@/*.aiv-delivery.net/
@@/*.amazonvideo.com/
@@/*.atv-ext-eu.amazon.com/
@@/*.atv-ext-fe.amazon.com/
@@/*.atv-ext.amazon.com/
@@/*.atv-ps.amazon.com/
@@/*.primevideo.com/
@@/*.pv-cdn.net/
@@||d25xi40x97liuc.cloudfront.net^
@@||dmqdd6hw24ucf.cloudfront.net^

! Apps (Mobile)
@@app.link
@@||mobilesdk-us.kampyle.com^
! sb.scorecardresearch.com

! Apple (https://support.apple.com/en-us/HT210060)
@@/*.aaplimg.com/
@@/*.appattest.apple.com/
@@/*.apple-dns.net/
@@/*.apple.com.akadns.net/
@@/*.apps.apple.com/
@@/*.business.apple.com/
@@/*.cdn-apple.com/
@@/*.icloud-apple.com.akadns.net/
@@/*.icloud-content.com/
@@/*.icloud.com.akadns.net/
@@/*.itunes-apple.com.akadns.net/
@@/*.itunes.apple.com/
@@/*.ls-apple.com.akadns.net/
@@/*.ls2-apple.com.akadns.net/
@@/*.mzstatic.com/
@@/*.origin-apple.com.akadns.net/
@@/*.push-apple.com.akadns.net/
@@/*.push.apple.com/
@@/*.safebrowsing.apple/
@@/*.school.apple.com/
@@||albert.apple.com^
@@||appldnld.apple.com^
@@apple.com
@@||bpapi.apple.com^
@@||captive.apple.com^
@@||configuration.apple.com^
@@||crl.apple.com^
@@||crl.entrust.net^
@@||crl3.digicert.com^
@@||crl4.digicert.com^
@@||cssubmissions.apple.com^
@@||deviceenrollment.apple.com^
@@||deviceservices-external.apple.com^
@@||diagassets.apple.com^
@@||doh.dns.apple.com^
@@||fba.apple.com^
@@||gdmf.apple.com^
@@||gg.apple.com^
@@||gnf-mdn.apple.com^
@@||gnf-mr.apple.com^
@@||gs.apple.com^
@@||humb.apple.com^
@@icloud.com
@@||identity.apple.com^
@@||ig.apple.com^
@@||iphone-ld.apple.com^
@@||iphone-ld.origin-apple.com.akadns.net^
@@||iprofiles.apple.com^
@@||isu.apple.com^
@@itunes.com
@@||lcdn-registration.apple.com^
@@||mdmenrollment.apple.com^
@@me.com
@@||mesu.apple.com^
@@||ns.itunes.apple.com^
@@||ocsp.apple.com^
@@||ocsp.digicert.com^
@@||ocsp.entrust.net^
@@||ocsp.verisign.net^
@@||ocsp2.apple.com^
@@||oscdn.apple.com^
@@||osrecovery.apple.com^
@@||ppq.apple.com^
@@||serverstatus.apple.com^
@@||setup.icloud.com^
@@||skl.apple.com^
@@||sq-device.apple.com^
@@||static.ips.apple.com^
@@||stats.gc.apple.com^
@@||swcdn.apple.com^
@@||swdist.apple.com^
@@||swdownload.apple.com^
@@||swpost.apple.com^
@@||swscan.apple.com^
@@||tbsc.apple.com^
@@||time-ios.apple.com^
@@||time-macos.apple.com^
@@||time.apple.com^
@@||updates-http.cdn-apple.com^
@@||updates.cdn-apple.com^
@@||valid.apple.com^
@@||vpp.itunes.apple.com^
@@||ws-ee-maidsvc.icloud.com^
@@||xp.apple.com^

! Cookies (Spiegel/...)
! *.mgr.consensu.org
@@/*.sp-prod.net/
@@||aggregator.service.usercentrics.eu^
@@||api.usercentrics.eu^
@@||apis.quantcast.mgr.consensu.org^
@@||app.usercentrics.eu^
@@||cdn.consentmanager.mgr.consensu.org^
@@||cdn.conversant.mgr.consensu.org^
@@||cdn.cookielaw.org^
@@||cdn.opencmp.net^
@@||cdn.privacy-mgmt.com^
@@||cmp-cdn.cookielaw.org^
@@||cmp.faktor.mgr.consensu.org^
@@consensu.org
@@||consent.cookiebot.com^
@@||consentcdn.cookiebot.com^
@@||consentcdn.cookiebot.com-v1.edgekey.net^
@@||consentmanager.mgr.consensu.org^
@@||consents.usercentrics.eu^
@@cookiebot.com
@@cookielaw.org
@@||cookies.onetrust.mgr.consensu.org^
@@||evidon.mgr.consensu.org^
@@||graphql.usercentrics.eu^
@@||iubenda.mgr.consensu.org^
@@||marfeel.mgr.consensu.org^
@@||message331.sp-prod.net^
@@||notice.sp-prod.net^
@@||pm.sourcepoint.mgr.consensu.org^
@@privacy-center.org
@@||quantcast.mgr.consensu.org^
@@||sdk-gcp.privacy-center.org^
@@||sdk.privacy-center.org^
@@||sibboventures.mgr.consensu.org^
@@||sourcepoint.mgr.consensu.org^
@@||spiegel-de.spiegel.de^
@@||static.quantcast.mgr.consensu.org^
@@||test.quantcast.mgr.consensu.org^
@@||vendorlist.consensu.org^

! Discord
@@/*.discord.com/
@@/*.discord.gg/
@@/*.discord.media/
@@/*.discordapp.com/
@@/*.discordapp.net/
@@/*.discordstatus.com/

! Disney+
@@/*.disney-plus.net/
@@/*.disneyplus.com/
@@/*.disneyplus.com.ssl.sc.omtrdc.net/
@@/*.dss.map.fastly.net/
@@/*.dssott.com/
@@/*.dssott.com.akamaized.net/
@@/*.search-api-disney.bamgrid.com/
@@||akamai-san24.exacttarget.com.edgekey.net^
@@||akamai-san39.exacttarget.com.edgekey.net^
@@||disney.demdex.net^
@@||e12906.dscx.akamaiedge.net^
@@||exacttarget.com.edgekey.net^
@@||image.mail.disneyplus.com^

! DNS
@@||whoami.akamai.net^

! Epic Games
@@/*.epicgames-download1.akamaized.net/
@@/*.epicgames-pubassets.akamaized.net/
@@/*.epicgames.com/
@@/*.epicgames.dev/
@@/*.epicgames.net/
@@/*.fortnite-vod.akamaized.net/
@@/*.unrealengine.com/
@@||catalogv2-svc-prod06-pub-772318179.us-east-1.elb.amazonaws.com^
@@||epicgames.map.fastly.net^

! Facebook
@@||connect.facebook.com^
@@||graph.facebook.com^
@@||graph.instagram.com^

! FAZ
@@||consent.faz.net^

! Gaming
! *.overwolf.com
! *.battle.net

! Github
@@/*.ghcr.io/
@@/*.github.com/
@@/*.github.map.fastly.net/
@@/*.githubapp.com/
@@/*.githubassets.com/
@@/*.githubusercontent.com/
@@||analytics-collector-28944298.us-east-1.elb.amazonaws.com^
@@||collector.githubapp.com^
@@github.io
@@||user-images.githubusercontent.com^

! Google
! *.googleapis.com
! app-measurement.com
@@||fundingchoicesmessages.google.com^
! geller-pa.googleapis.com
! google-analytics.com
! googletagmanager.com
! googletagservices.com
@@||people-pa.googleapis.com^
! ssl.google-analytics.com
! www-google-analytics.l.google.com
! www-googletagmanager.l.google.com
! www.google-analytics.com
! www.googletagmanager.com
! www.googletagservices.com
@@||id.google.com^

! Hulu
@@||dpm.demdex.net^

! Images
@@mycliplister.com
@@||assets.mmsrg.com^

! Kicker Mobile App
@@||display.apester.com^
@@||events.apester.com^
@@||img.apester.com^
@@||interaction.apester.com^
@@||renderer.apester.com^
@@||static.apester.com^

! Microsoft Windows 10 - https://docs.microsoft.com/de-de/windows/privacy/manage-windows-2004-endpoints
@@/*.delivery.mp.microsoft.com/
@@/*.dl.delivery.mp.microsoft.com/
@@/*.prod.do.dsp.mp.microsoft.com/
@@/*.update.microsoft.com/
@@/*.windowsupdate.com/
@@/*.wns.windows.com/
@@||adl.windows.com^
@@||arc.msn.com^
@@||blob.weather.microsoft.com^
@@||blobs.officehome.msocdn.com^
@@||cdn.onenote.net^
@@||checkappexec.microsoft.com^
@@||config.edge.skype.com^
@@||config.teams.microsoft.com^
@@||ctldl.windowsupdate.com^
@@||displaycatalog.mp.microsoft.com^
@@||dlassets-ssl.xboxlive.com^
@@||dmd.metaservices.microsoft.com^
@@||emdl.ws.microsoft.com^
@@||evoke-windowsservices-tas.msedge.net^
@@||fs.microsoft.com^
@@||g.live.com^
@@||go.microsoft.com^
@@||img-prod-cms-rt-microsoft-com.akamaized.net^
@@||licensing.mp.microsoft.com^
@@||login.live.com^
@@||logincdn.msauth.net^
@@||manage.devcenter.microsoft.com^
@@||maps.windows.com^
@@office.com
@@||oneclient.sfx.ms^
@@||ow1.res.office365.com^
@@||self-events-data.trafficmanager.net^
@@||self.events.data.microsoft.com^
@@||settings-win.data.microsoft.com^
@@||storecatalogrevocation.storequality.microsoft.com^
@@||tile-service.weather.microsoft.com^
@@||tsfe.trafficshaping.dsp.mp.microsoft.com^
@@||v10.events.data.microsoft.com^
@@||v20.events.data.microsoft.com^
@@||wdcp.microsoft.com^
@@||www.msftconnecttest.com^

! Microsoft
@@/*.aria.microsoft.com/
@@/*.data.microsoft.com/
@@/*.teams.microsoft.com/
@@/*.visualstudio.com/
@@||codepush.appcenter.ms^
@@||codepush.teams.microsoft.com^
@@||codepush.trafficmanager.net^
@@||dc.services.visualstudio.com^
@@||g.msn.com^
@@||global.in.ai.monitor.azure.com^
@@||ic3.events.data.microsoft.com^
@@||mobile.pipe.aria.microsoft.com^
@@||prod.rewardsplatform.microsoft.com^
@@||teams.events.data.microsoft.com^
@@||vortex-win.data.microsoft.com^
@@||vortex.data.microsoft.com^
@@||web.vortex.data.microsoft.com^

! Netflix
@@/*.netflix.com/
@@/*.netflix.net/
@@/*.nflxext.com/
@@/*.nflximg.com/
@@/*.nflximg.net/
@@/*.nflxso.net/
@@/*.nflxvideo.net/
@@||apiproxy-device-prod-nlb-3-a653f8a785200e05.elb.us-west-2.amazonaws.com^
@@||customerevents.netflix.com^
@@||dualstack.apiproxy-device-prod-nlb-1-4d12762d4ba53e45.elb.eu-west-1.amazonaws.com^
@@||dualstack.apiproxy-device-prod-nlb-1-c582e7914e487bf4.elb.us-east-1.amazonaws.com^
@@||dualstack.apiproxy-device-prod-nlb-2-300c995e1ce8a001.elb.us-east-1.amazonaws.com^
@@||dualstack.apiproxy-device-prod-nlb-3-a6ae1986950e693f.elb.us-east-1.amazonaws.com^
@@||dualstack.apiproxy-device-prod-nlb-3-d601e17f0cb54f72.elb.eu-west-1.amazonaws.com^
@@||dualstack.apiproxy-device-prod-nlb-4-1f9e6a56738a49ec.elb.us-east-1.amazonaws.com^
@@||dualstack.apiproxy-device-prod-nlb-4-f1dce6fa09ac5989.elb.eu-west-1.amazonaws.com^
@@||dualstack.apiproxy-http1-199106617.us-east-1.elb.amazonaws.com^
@@||dualstack.beaconserver-ce-vpc0-1537565064.eu-west-1.elb.amazonaws.com^
@@||dualstack.ichnaea-web-121909266.us-east-1.elb.amazonaws.com^
@@||ichnaea-web.netflix.com^
@@||ichnaea.netflix.com^
@@||nrdp.prod.ftl.netflix.com^
@@||push.prod.netflix.com^

! Nintendo
@@/*.nintendo.com/
@@/*.nintendo.net/
@@/*.nintendowifi.net/
@@||a1843.g.akamai.net^
@@||image.ccg.nintendo.com^
@@||image.ccg.nintendo.net^

! n-tv
@@/*.damoh.n-tv.de/
@@/*.ntvde.damoh.schneevonmorgen.com/
@@||1.ntvde.damoh.schneevonmorgen.com^
@@||2.ntvde.damoh.schneevonmorgen.com^
@@||ssl.1.damoh.n-tv.de^
@@||ssl.2.damoh.n-tv.de^

! NVidia
@@||events.gfe.nvidia.com^
@@||gfe.nvidia.com^
@@||gfwsl.geforce.com^
@@||images.nvidiagrid.net^
@@||services.gfe.nvidia.com^

! Newsportale
@@||api.opinary.com^
@@||tag.aticdn.net^

! Paypal
@@/*.braintreegateway.com/
@@/*.paypal-dynamic-2.map.fastly.net/
@@/*.paypal.ca/
@@/*.paypal.co.uk/
@@/*.paypal.com/
@@/*.paypal.map.fastly.net/
@@/*.paypalobjects.com/
@@||t.paypal.com^

! Referrer (Clicks)
@@||a.nonstoppartner.net^
@@||ad.doubleclick.net^
@@||adservice.google.com^
@@||adservice.google.de^
@@||clickserve.dartsearch.net^
@@googleadservices.com
@@||m.exactag.com^
@@||mydealz.digidip.net^
@@||pagead46.l.doubleclick.net^
@@||pixel.everesttech.net^
@@||promo.mobile.de^
@@||t.myvisualiq.net^
@@||tp-emea.exactag.com^
@@||trk.klclick.com^
@@||www.googleadservices.com^

! Shopping
@@||content.aimatch.com^
@@||widgets.trustedshops.com^

! Steam
@@/*.s.team/
@@/*.steam-chat.com/
@@/*.steambroadcast.akamaized.net/
@@/*.steamcdn-a.akamaihd.net/
@@/*.steamchina.com/
@@/*.steamcommunity-a.akamaihd.net/
@@/*.steamcommunity.com/
@@/*.steamcontent.com/
@@/*.steamgames.com/
@@/*.steampowered.com/
@@/*.steampowered.com.8686c.com/
@@/*.steamstatic.com/
@@/*.steamstatic.com.8686c.com/
@@/*.steamstore-a.akamaihd.net/
@@/*.steamusercontent-a.akamaihd.net/
@@/*.steamusercontent.com/
@@/*.steamuserimages-a.akamaihd.net/
@@/*.steamvideo-a.akamaihd.net/
@@/*.valvesoftware.com/

! Sony (Playstation)
@@/*.playstation.com/
@@/*.playstation.com.ssl.d1.sc.omtrdc.net/
@@/*.playstation.net/
@@/*.sonycoment.loris-e.llnwd.net/
@@/*.sonyentertainmentnetwork.com/
@@/*.sonyjpnpsg-1.hs.llnwd.net/

! Spotify
@@/*.audio-ak-spotify-com.akamaized.net/
@@/*.audio-akp-bbr-spotify-com.akamaized.net/
@@/*.audio4-ak-spotify-com.akamaized.net/
@@/*.heads-ak-spotify-com.akamaized.net/
@@/*.pscdn.co/
@@/*.scdn.co/
@@/*.spoti.fi/
@@/*.spotify-com.akamaized.net/
@@/*.spotify.com/
@@/*.spotify.com.edgesuite.net/
@@/*.spotify.edgekey.net/
@@/*.spotify.map.fastly.net/
@@/*.spotifycdn.com/
@@/*.spotifycdn.map.fastly.net/
@@/*.spotifycdn.net/
@@/*.spotifycharts.com/
@@/*.spotifycodes.com/
@@/*.spotifyjobs.com/
@@/*.spotilocal.com/
@@||spotify.demdex.net^
@@||wl.spotify.com^

! Streaming/Videos
@@||cdn-gl.imrworldwide.com^
@@||cdn2.cam-content.com^
@@||cws-eu.conviva.com^
@@||ht-cdn2.adtng.com^
@@||hw-cdn.trafficjunky.net^
@@||hw-cdn2.adtng.com^
@@||hw-cdn2.trafficjunky.net^
@@||js-sec.indexww.com^
@@||player-feedback-v1.glomex.com^
@@||player.glomex.com^
@@||redefine.hit.stat24.com^
@@||s0-2mdn-net.l.google.com^
@@||s0.2mdn.net^
@@||s2.static.cfgr3.com^
@@||s3t3d2y7.ackcdn.net^
@@||uim.tifbs.net^
@@||video.xxxjmp.com^
@@||videos-fms.jwpsrv.com^
@@||vz-cdn.trafficjunky.net^
@@||vz-cdn2.adtng.com^
@@||vz-cdn2.trafficjunky.net^

! Twitch
@@/*.ext-twitch.tv/
@@/*.jtvnw.net/
@@/*.live-video.net/
@@/*.ttvnw.net/
@@/*.twitch.map.fastly.net/
@@/*.twitch.tv/
@@/*.twitchcdn.net/
@@/*.twitchsvc.net/
@@||countess-prod-public-176850629.us-west-2.elb.amazonaws.com^
@@||countess.twitch.tv^
@@ink1001.com
@@||mi.twitch.tv^
@@movable-ink-397.com
@@||science-edge-external-prod-73889260.us-west-2.elb.amazonaws.com^
@@||static-cdn.jtvnw.net^
@@||supervisor.ext-twitch.tv^

! Twitter
@@||analytics.twitter.com^
@@||syndication.twitter.com^

! Verschiedene
! *.cleverpush.com
! *.crashlytics.com
@@/*.sentry.io/
@@||cdn.optimizely.com^

! WhatsApp
! *.whatsapp.com
! *.whatsapp.net

! Webex
@@/*.ciscospark.com/
@@/*.wbx2.com/
@@/*.webex.com/
@@/*.webex.com.cn/
@@/*.webex.com.edgekey.net/
@@/*.webex.com.ssl.sc.omtrdc.net/
@@||admin-webex-com-690861900.us-east-2.elb.amazonaws.com^

! XBox
@@/*.xbox.com/
@@/*.xboxab.com/
@@/*.xboxab.net/
@@/*.xboxlive.com/
@@/*.xboxlive.com.akadns.net/
@@/*.xboxlive.com.c.footprint.net/
@@/*.xboxlive.com.edgekey.net/
@@/*.xboxservices.com/
@@||click.engage.xbox.com^
@@||image.engage.xbox.com^

! Youtube
@@/*.googlevideo.com/
@@/*.gvt1.com/
@@/*.gvt2.com/
@@/*.video.google.com/
@@/*.youtube-ui.l.google.com/
@@/*.youtube.googleapis.com/
@@/*.youtubeeducation.com/
@@/*.youtubei.googleapis.com/
@@/*.yt3.ggpht.com/
@@/*.ytimg.com/
@@||s.youtube.com^
@@youtu.be
@@youtube-nocookie.com
@@youtube.com

! Zertifikate
@@/*.lencr.org/
@@||r3.o.lencr.org^

! Zoom
@@/*.zoom.us/
@@/*.zoomgov.com/
@@||click.zoom.us^
@@||log.zoom.us^
@@||logfiles.zoom.us^
